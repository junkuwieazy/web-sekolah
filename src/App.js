import React, { Fragment } from 'react'
import {BrowserRouter, Route} from 'react-router-dom'

import 'bootstrap/dist/css/bootstrap.min.css';
import './assets/css/style.css'

import Header from './templates/front/Header';
import Footer from './templates/front/Footer';

import Home from './pages/Front/Home'
import Artikel from './pages/Front/Artikel';
import Tentang from './pages/Front/Tentang';
import Guru from './pages/Front/Guru';
import VisiMisi from './pages/Front/VisiMisi';


function App() {
  return (
    <BrowserRouter>
      <Fragment>
        <Header />
        <Route path="/" exact component={Home} />
        <Route path="/tentang" exact component={Tentang} />
        <Route path="/artikel" exact component={Artikel} />
        <Route path="/guru" exact component={Guru} />
        <Route path="/jurusan" exact component={Guru} />
        <Route path="/ekskul" exact component={Guru} />
        <Route path="/visimisi" exact component={VisiMisi} />
        <Footer />
      </Fragment>
    </BrowserRouter>
  );
}

export default App;
