import React from 'react';
import { Nav } from "react-bootstrap";
import { LinkContainer } from "react-router-bootstrap";
import logoMI from '../../assets/images/logo-madrasah.png'

const Header = () => {
    return (
        <>
            <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-success">
                <div className="container">
                    {/* <a className="navbar-brand" href="#/">Fixed navbar</a> */}
                    <a className="navbar-brand" href="#/">
                        <img src={logoMI} width="50" alt="Logo Sekolah" />
                    </a>
                    <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
                        <span className="navbar-toggler-icon"></span>
                    </button>
                    <div className="collapse navbar-collapse" id="navbarCollapse">
                        <ul className="navbar-nav me-auto mb-2 mb-md-0">
                            <LinkContainer exact to="/">
                                <Nav.Link>Home</Nav.Link>
                            </LinkContainer>

                            <LinkContainer exact to="/tentang">
                                <Nav.Link>Tentang Kami</Nav.Link>
                            </LinkContainer>

                            <LinkContainer exact to="/artikel">
                                <Nav.Link>Artikel</Nav.Link>
                            </LinkContainer>

                            <LinkContainer exact to="/guru">
                                <Nav.Link>Guru</Nav.Link>
                            </LinkContainer>

                            <LinkContainer exact to="/jurusan">
                                <Nav.Link>Jurusan</Nav.Link>
                            </LinkContainer>

                            <LinkContainer exact to="/ekskul">
                                <Nav.Link>Ekskul</Nav.Link>
                            </LinkContainer>

                            <LinkContainer exact to="/visimisi">
                                <Nav.Link>Visi & Misi</Nav.Link>
                            </LinkContainer>

                            {/* <LinkContainer exact to="/bukutamu">
                                <Nav.Link>Buku Tamu</Nav.Link>
                            </LinkContainer> */}

                            <LinkContainer exact to="/login">
                                <Nav.Link>Login</Nav.Link>
                            </LinkContainer>
                        </ul>
                        <form className="d-flex">
                            <div className="input-group col-md-4">
                                <input className="form-control py-2 border-right-0 border" type="search" placeholder="Pencarian" id="example-search-input" />
                            </div>
                        </form>
                    </div>
                </div>
            </nav>
        </>
    );
}

export default Header;