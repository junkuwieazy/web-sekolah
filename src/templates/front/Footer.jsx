import React from 'react';

const Footer = () => {
    return (
        <>
            <div className="container">
                <footer className="py-3 my-4">
                    <ul className="nav justify-content-center border-bottom pb-3 mb-3">
                        <li className="nav-item"><a href="/" className="nav-link px-2 text-muted">Home</a></li>
                        <li className="nav-item"><a href="/tentang" className="nav-link px-2 text-muted">Tentang Kami</a></li>
                        <li className="nav-item"><a href="/artikel" className="nav-link px-2 text-muted">artikel</a></li>
                        <li className="nav-item"><a href="/guru" className="nav-link px-2 text-muted">Guru</a></li>
                        <li className="nav-item"><a href="/jurusan" className="nav-link px-2 text-muted">Jurusan</a></li>
                        <li className="nav-item"><a href="/ekskul" className="nav-link px-2 text-muted">Ekskul</a></li>
                        <li className="nav-item"><a href="/visimisi" className="nav-link px-2 text-muted">Visi & Misi</a></li>
                        {/* <li className="nav-item"><a href="/bukutamu" className="nav-link px-2 text-muted">Buku Tamu</a></li> */}
                        <li className="nav-item"><a href="/login" className="nav-link px-2 text-muted">Login</a></li>
                    </ul>
                    <p className="text-center text-muted">© 2021 Universitas Pamulang</p>
                </footer>
            </div>
        </>
    );
}

export default Footer;