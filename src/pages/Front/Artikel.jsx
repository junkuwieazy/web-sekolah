import React from 'react';
import RightSide from './includes/RightSide';

const Artikel = () => {

    return (
        <>
            <main className="container">
                <div className="bg-light">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="pembatas">Artikel</div>
                            <div className="row">
                                <div className="col-md-4">
                                    <div className="card mb-4 box-shadow">
                                        <img
                                            className="card-img-top card-imgs"
                                            src="https://i.picsum.photos/id/1026/4621/3070.jpg?hmac=OJ880cIneqAKIwHbYgkRZxQcuMgFZ4IZKJasZ5c5Wcw"
                                            alt="Third slide"
                                        />
                                        <div className="card-body">
                                            <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="btn-group">
                                                    <small className="text-muted">3.000 view</small>
                                                </div>
                                                <small className="text-muted">09/10/2021</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="card mb-4 box-shadow">
                                        <img
                                            className="card-img-top card-imgs"
                                            src="https://i.picsum.photos/id/1036/4608/3072.jpg?hmac=Tn9CS_V7lFSMMgAI5k1M38Mdj-YEJR9dPJCyeXNpnZc"
                                            alt="Third slide"
                                        />
                                        <div className="card-body">
                                            <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="btn-group">
                                                    <small className="text-muted">3.000 view</small>
                                                </div>
                                                <small className="text-muted">09/10/2021</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-4">
                                    <div className="card mb-4 box-shadow">
                                        <img
                                            className="card-img-top card-imgs"
                                            src="https://i.picsum.photos/id/1031/5446/3063.jpg?hmac=Zg0Vd3Bb7byzpvy-vv-fCffBW9EDp1coIbBFdEjeQWE"
                                            alt="Third slide"
                                        />
                                        <div className="card-body">
                                            <p className="card-text">This is a wider card with supporting text below as a natural lead-in to additional content. This content is a little bit longer.</p>
                                            <div className="d-flex justify-content-between align-items-center">
                                                <div className="btn-group">
                                                    <small className="text-muted">3.000 view</small>
                                                </div>
                                                <small className="text-muted">09/10/2021</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <RightSide />
                    </div>
                </div>
            </main>
        </>
    )
}

export default Artikel;