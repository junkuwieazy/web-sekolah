import React from 'react';
import RightSide from './includes/RightSide';

const Tentang = () => {

    return (
        <>
            <main className="container">
                <div className="bg-light">
                    <div className="row">
                        <div className="col-md-8">
                            <img
                                className="card-img-top card-imgs-tentang"
                                src="https://i.picsum.photos/id/1031/5446/3063.jpg?hmac=Zg0Vd3Bb7byzpvy-vv-fCffBW9EDp1coIbBFdEjeQWE"
                                alt="Third slide"
                            />
                            <h2>Nama Sekolah</h2>
                            <p className="fs-5 text-just">
                                Quickly and easily get started with Bootstrap's compiled,
                                production-ready files with this barebones example featuring some basic HTML and helpful links.
                                Download all our examples to get started
                            </p>
                        </div>
                        <RightSide />
                    </div>
                </div>
            </main>
        </>
    );
}

export default Tentang;