import React from 'react';
import RightSide from './includes/RightSide';

const Guru = () => {

    return (
        <>
            <main className="container">
                <div className="bg-light">
                    <div className="row">
                        <div className="col-md-8">
                            <div className="pembatas">Daftar Guru</div>
                            <div className="row">
                                <div className="col-md-3">
                                    <div className="card mb-4 box-shadow">
                                        <img
                                            className="card-img-top card-imgs-guru"
                                            src="https://i.picsum.photos/id/1026/4621/3070.jpg?hmac=OJ880cIneqAKIwHbYgkRZxQcuMgFZ4IZKJasZ5c5Wcw"
                                            alt="Third slide"
                                        />
                                        <div className="card-body">
                                            <p className="card-text">
                                                <b>Nama</b> : Pandu Wiliantoro
                                                <br/>
                                                <br/>
                                                <b>Mata Pelajaran</b> : This is a wider card with supporting text
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="card mb-4 box-shadow">
                                        <img
                                            className="card-img-top card-imgs-guru"
                                            src="https://i.picsum.photos/id/1026/4621/3070.jpg?hmac=OJ880cIneqAKIwHbYgkRZxQcuMgFZ4IZKJasZ5c5Wcw"
                                            alt="Third slide"
                                        />
                                        <div className="card-body">
                                            <p className="card-text">
                                                <b>Nama</b> : Pandu Wiliantoro
                                                <br/>
                                                <br/>
                                                <b>Mata Pelajaran</b> : This is a wider card with supporting text
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="card mb-4 box-shadow">
                                        <img
                                            className="card-img-top card-imgs-guru"
                                            src="https://i.picsum.photos/id/1026/4621/3070.jpg?hmac=OJ880cIneqAKIwHbYgkRZxQcuMgFZ4IZKJasZ5c5Wcw"
                                            alt="Third slide"
                                        />
                                        <div className="card-body">
                                            <p className="card-text">
                                                <b>Nama</b> : Pandu Wiliantoro
                                                <br/>
                                                <br/>
                                                <b>Mata Pelajaran</b> : This is a wider card with supporting text
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-3">
                                    <div className="card mb-4 box-shadow">
                                        <img
                                            className="card-img-top card-imgs-guru"
                                            src="https://i.picsum.photos/id/1026/4621/3070.jpg?hmac=OJ880cIneqAKIwHbYgkRZxQcuMgFZ4IZKJasZ5c5Wcw"
                                            alt="Third slide"
                                        />
                                        <div className="card-body">
                                            <p className="card-text">
                                                <b>Nama</b> : Pandu Wiliantoro
                                                <br/>
                                                <br/>
                                                <b>Mata Pelajaran</b> : This is a wider card with supporting text
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <RightSide />
                    </div>
                </div>
            </main>
        </>
    );
}

export default Guru;